$(function () {
  $('#modal-open').click(function() {
    $(this).blur();
    $('#modal-overlay').fadeIn();
    $('#modal-content').fadeIn();
    centeringModal();
    noScroll();
  });

  $("#modal-overlay, #modal-close").unbind().click(function(){
    $('#modal-overlay').fadeOut();
    $('#modal-content').fadeOut();
    returnScroll();
  });

  // 問い合わせフォームの項目の高さを調整。
  $(window).load(function() {
    resizeHeight();
  });

  // モーダルウィンドウの位置調整
  $(window).resize(function() {
      centeringModal();
      resizeHeight();
  });
});

// モーダルウィンドウを常に画面中央に表示する。
function centeringModal() {
  var w = $(window).width();
  var h = $(window).innerHeight();

  var cw = $("#modal-content").outerWidth(true);
  var ch = $("#modal-content").outerHeight(true);

  var left = ((w - cw) / 2);
  var top = ((h - ch) / 2);

  $("#modal-content").css('top', top + 'px');
  $("#modal-content").css('left', left + 'px');
}

function resizeHeight() {
  if ($(window).width() < 639){
    $("div.mw_wp_form form dl>*").removeAttr('style');
  } else {
    $("div.mw_wp_form form dl>*:not(:last-child,:nth-last-child(2))").heightLine();
    $("div.mw_wp_form form dl>*:last-child,div.mw_wp_form form dl>*:nth-last-child(2)").heightLine();
  }
}

//スクロール禁止用関数
function noScroll(){
  //PC用
  var scroll_event = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
  $(document).on(scroll_event,function(e){e.preventDefault();});
  //SP用
  $(document).on('touchmove.noScroll', function(e) {e.preventDefault();});
}

//スクロール復活用関数
function returnScroll(){
  //PC用
  var scroll_event = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
  $(document).off(scroll_event);
  //SP用
  $(document).off('.noScroll');
}
