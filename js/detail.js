var flg = "close";
jQuery(function() {
  jQuery('#view_detail').click(function() {
    var button = jQuery('#basic_info');
    button.slideToggle();
    if (flg == 'close') {
      jQuery("#view_detail").text('Close');
      jQuery("#view_detail").removeClass('open');
      jQuery("#view_detail").addClass('close');
      flg = 'open';
    } else {
      jQuery("#view_detail").text('View');
      jQuery("#view_detail").removeClass('close');
      jQuery("#view_detail").addClass('open');
      flg = 'close';
    }
  });
});
