$(function () {
  // Loading中
    $('div.contents').css('display', 'none');

    var loading_bg = $('<div></div>', {
      id:'loadging-bg'
    });
    var loading = $('<div></div>', {
      id:"loading"
    });
    loading_bg.append(loading);
    var img_dir = getHost() + "/wp-content/themes/bennie-a/img/";
    var img = $('<img>', {
      src: img_dir + "loading.gif"
    });
    loading.append(img);
    loading.append('<p>Loading</p>');
    $('body').append(loading_bg);

    // Page Top機能
    $('.page_top').click(function () {
        $('body, html').animate({
            scrollTop: 0
        }, 700);
        return false;
    });

    var path = getHost() + '/#';
    $("a[href^='" + path + "']").click(function() {
      var href = $(this).attr('href');
      var target = $(href.replace(getHost() + "/", ''));
      // 移動先を数値で取得
      var position = target.offset().top - $('#top-head').innerHeight();
      // // スムーススクロール
      $('body,html').animate({scrollTop:position}, 700, 'swing');
      return false;
    });

    $(".thumbnail").hover(
        function () {
            var $txt = $(this).find(".txt");
            var $p = $txt.find("p").css({
                "opacity": 0,
                "top": 15
            });
            var $a = $txt.find("a").css({
                "opacity": 0,
                "bottom": 0
            });
            $txt.stop().fadeTo(120, 0.85);
            $p.stop().delay(130).animate({
                "opacity": 1,
                "top": 0
            }, 180, "easeOutExpo");
            $a.stop().delay(250).animate({
                "opacity": 1,
                "bottom": 20
            }, 180, "easeOutExpo");
        },
        function () {
            var $txt = $(this).find(".txt");
            $txt.find("p").stop().delay(0).animate({
                "opacity": 0,
                "top": -15
            }, 250, "easeOutExpo");
            $txt.find("a").stop().delay(100).animate({
                "opacity": 0,
                "bottom": 30
            }, 250, "easeOutExpo");
            $txt.stop().delay(300).fadeOut(180);
        }
    );

    var now = new Date();
    $("#copyright").html("&copy; " + now.getFullYear() + " Aiko Beniyama");
});


$(window).load(function () { //全ての読み込みが完了したら実行
  $('#loading').fadeOut(800);
  $('#loadging-bg').fadeOut(300);
  $('div.contents').css('display', 'block');
});

function getHost() {
  var host = window.location.hostname;
  if (host == 'localhost') {
    host = host + '/bitbucket/portfolio'
  } else {
    host = 'bennie-a.com';
  }
  return 'http://' + host;
}
