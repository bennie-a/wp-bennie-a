jQuery(function () {
    var container = jQuery(".photos");
    var html = "";
    jQuery('.loading').show();
    jQuery.ajax({
        url: ajaxurl, //PHPファイルのURL
        dataType: "json"
    }).done(function (data) {
        //通信成功時の処理
        jQuery.each(data.data, function (i, item) {
            var imgurl = item.images.standard_resolution.url.split('?')[0]; //低解像度の画像のURLを取得
            var text = item.caption.text;
            var link = item.link; //リンクを取得
            html += "<li class='banner banner-view'><img src='" + imgurl + "'><a href='" + imgurl + "'  data-gallery='gallery' class='boxer'><div class='mask'><p>Read more...</p></div></a></li>";
        });
    }).fail(function () {
        //通信失敗時の処理
        html = "<li>画像を取得できません。</li>";
    }).always(function () {
        //通信完了時の処理
        jQuery('.loading').hide();
        container.html(html);
        jQuery(".boxer").boxer();
    });
});
