<?php
// <title>タグ表示
function theme_slug_setup() {
    add_theme_support('title-tag');
}

add_action('after_setup_theme', 'theme_slug_setup');

function title_separator($sep) {
    $sep = '|';
    return $sep;
}

add_filter('document_title_separator', 'title_separator');

// ヘッダー、フッターの画像パスを表示
function common_img_path() {
    return get_template_directory_uri().'/img/';
}

//js読み込み
add_action( 'wp_enqueue_scripts', 'my_script_output');
function my_script_output() {
    wp_enqueue_script( 'menu-js', get_template_directory_uri() . '/js/global-menu.js', array( 'jquery') );
    wp_enqueue_script( 'my-script', get_template_directory_uri() . '/js/common.js', array( 'jquery') );
    if (is_front_page()) {
      wp_enqueue_script( 'index-js', get_template_directory_uri() . '/js/index.js', array( 'jquery') );
        wp_enqueue_script( 'boxer-js', get_template_directory_uri() . '/js/Boxer-master/jquery.fs.boxer.min.js', array( 'jquery') );
    } else if (is_single()) {
      wp_enqueue_script( 'detail-js', get_template_directory_uri() . '/js/detail.js', array( 'jquery') );
    } else if (get_post(get_the_ID())->post_name == 'contact') {
      wp_enqueue_script( 'heightLine', get_template_directory_uri() . '/js/jquery.heightLine.js', array( 'jquery') );
      wp_enqueue_script( 'contact-js', get_template_directory_uri() . '/js/contact.js', array( 'jquery') );
    }
}

function my_styles() {
    if (is_front_page()){
      wp_enqueue_style( 'index-css', get_bloginfo( 'stylesheet_directory') . '/css/index.css', array(), null, 'all');
      wp_enqueue_style( 'boxer-css', get_bloginfo( 'stylesheet_directory') . '/js/Boxer-master/jquery.fs.boxer.min.css', array(), null, 'all');
    } else if (is_single()) {
        wp_enqueue_style( 'detail-css', get_bloginfo( 'stylesheet_directory') . '/css/detail.css', array(), null, 'all');
    } else {
        wp_enqueue_style( 'fixed-css', get_bloginfo( 'stylesheet_directory') . '/css/fixed.css', array(), null, 'all');
    }
}
add_action( 'wp_enqueue_scripts', 'my_styles');

function ajax_url() {
    if (is_front_page()):
?>
    <script>
        var ajaxurl = '<?php echo get_template_directory_uri().'/instagram_api.php'; ?>';
    </script>
<?php
    endif;
}
add_action('wp_head', 'ajax_url', 1);
//以下ダッシュボード関連
// SVGファイルに対応する
add_filter( 'upload_mimes', 'my_add_mime_type' );
if( !function_exists('my_add_mime_type') ) {
	function my_add_mime_type($mime_types)
	{
		$mime_types['svg'] = 'image/svg+xml';
		$mime_types['svgz'] = 'image/svg+xml';
		return $mime_types;
	}
}
add_filter('ext2type', 'my_ext2type');
if( !function_exists('my_ext2type') ) {
	function my_ext2type($ext2types)
	{
		array_push($ext2types['image'], 'svg', 'svgz');
		return $ext2types;
	}
}
add_filter('wp_generate_attachment_metadata', 'my_wp_generate_attachment_metadata',1,2);
if( !function_exists('my_wp_generate_attachment_metadata') ) {
	function my_wp_generate_attachment_metadata($metadata, $attachment_id)
	{
		$attachment_post = $post = get_post($attachment_id);
		$type = $attachment_post->post_mime_type;
		if ($type === 'image/svg+xml' && empty($metadata)) {
			$upload_dir = wp_upload_dir();
			$base_name = basename($attachment_post->guid);
			$metadata = array(
				'file' => $upload_dir['subdir'] . '/' . $base_name
			);
		}
		return $metadata;
	}
}

// カスタムヘッダー有効
add_theme_support('custom-header', array(
    'width' => 1120,
    'height' => 580,
    'header-text' => false
));
//アイキャッチ有効
add_theme_support('post-thumbnails');
register_nav_menu('sitenav', 'サイトナビゲーション');

// トップページのPortfolio一覧のサムネイル
set_post_thumbnail_size(240, 216, true);
// トップページのPortfolio一覧
function get_thumbnail($size) {
    $url = get_template_directory_uri().'/picnic.jpg';
    // if (has_post_thumbnail()) {
    //   $postthumb = wp_get_attachment_image_src(get_post_thumbnail_id(), $size);
    //   $url = $postthumb[0];
    // }
    return $url;
}

// 投稿IDからページリンクを作成。
function get_post_link($post_id) {
  $title = get_the_title($post_id);
  $title_block = '<div><cite>'.$title.'</cite><span class="move">View Project</span>'.'</div>';
  $img = '<img src="'.get_post_thumb_url($post_id).'" alt="'.$title.'"/>';
  return '<a class="portfolio-link" href="'.get_permalink($post_id).'">'.$img.$title_block.'</a>';
}

// 投稿IDからサムネイル画像を取得する
function get_post_thumb_url($post_id) {
  return wp_get_attachment_url(get_post_thumbnail_id($post_id));
}
