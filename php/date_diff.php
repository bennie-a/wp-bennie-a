<?php
    $current = new DateTime(date('Y-m-d h:i:s'));
    $start = new DateTime('2007-04-01 00:00:00');
    $interval = $start->diff($current);
    header('Content-type:application/json; charset=utf8');
    echo json_encode(array('date' => $interval->format('%y年%mヶ月%d日'),
                           'hour' => $interval->h, 'min' => $interval->i,
                           'sec' => $interval->s));
?>