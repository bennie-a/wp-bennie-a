<footer>
    <div class="contact">
        <h2>ベニヤマとちょっとお話をしてみませんか?</h2>
        <ul>
            <li>
                <a href="contact/index.php"><i class="fa fa-envelope"></i></a>

            </li>
            <li>
                <a href="https://twitter.com/bennie_a84/" target="_blank"><i class="fa fa-twitter"></i></a>
            </li>
            <li>
                <a href="https://instagram.com/bennie_a84/" target="_blank"><i class="fa fa-instagram"></i></a>
            </li>
            <li>
                <a href="https://github.com/bennie-a" target="_blank"><i class="fa fa-github"></i></a>
            </li>
        </ul>
    </div>
    <div class="page_top">
        <p>
            <a href="" id="to_top"></a>
        </p>
    </div>
    <div class="footer">
        <small id="copyright"></small>
        <nav role="navigation">
            <?php wp_nav_menu(array(
                'theme_location' => 'sitenav'
            )); ?>
        </nav>
    </div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>
