<?php
/*
Template Name: 固定ページ
*/
?>
<?php get_header(); ?>
    <div class="contents">
        <main role="main">
           <div class="hero">
              <?php if(has_post_thumbnail()): ?>
                <div class="hero-img" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id()); ?>);">
                  <?php the_title('<h1>', '</h1>'); ?>
                </div>
              <?php endif; ?>
           </div>
          <?php if(have_posts()): while(have_posts()): the_post(); ?>
              <?php the_content(); ?>
          <?php endwhile; endif; ?>
        </main>
        <?php get_footer(); ?>
