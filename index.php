<?php get_header(); ?>
     <div class="contents">
        <main role="main">
          <div class="hero">
               <?php if(get_header_image()): ?>
               <div class="hero-img" style="background-image:url(<?php header_image(); ?>)"></div>
               <?php endif; ?>
               <div class="hero-text">
                   <span class="code">Code</span>&nbsp;And&nbsp;<span class="design">Design</span>
               </div>
          </div>
           <section id="message">
                <h2>こんにちは。<span>ベニヤマアイコです。</span></h2>
                <div>
                  <p>8年間Webエンジニアをしていました。<br>しかしエンジニアとデザイナーの見えない壁を取り払うためWebクリエイターに転向することにしました。</p>
                </div>
            </section>
           <section id="portfolio">
                <?php
                  $i = 0;
                  if (have_posts()):?>
                  <ul>
                  <?php while (have_posts()):
                          the_post();
                          if ($i > 10) {
                            break;
                          };
                  　?>
                  <li>
                      <a href="<?php the_permalink() ?>" class="portfolio-link">
                        <div class="">
                          <ul class="tag">
                            <?php
                              $posttags = get_the_tags();
                              if ($posttags) {
                                foreach ($posttags as $tag) {
                                  echo '<li>'.$tag->name.'</li>';
                                }
                              }
                            ?>
                          </ul>
                          <cite><?php echo the_title(); ?></cite><span class="move">View Project</span>
                        </div>
                        <?php
                          if (has_post_thumbnail()): the_post_thumbnail('full');?>
                        <?php endif; ?>
                    </a>
                  </li>
                  <?php $i++; endwhile; ?>
                </ul>
              <?php else: ?>
                <p>
                  ただいま準備中です。
                </p>
                <?php endif; ?>
                <?php wp_reset_postdata(); wp_reset_query(); ?>
          </section>
           <section id="about_me">
                <h2>About Me</h2>
                <p>プログラムが書けます。イラストやアイコン、ロゴを描くのは楽しいです。Webサイトのコンセプトやデザインについて考えるとワクワクします。考えに行き詰まったら散歩やヨガなどで体を動かします。結婚してます。旅行先では猫グッズを探します。プラモデルを作って遊んでます。女性です。</p>
                <!-- <div><a href="about_me.html" class="ghost ghost_white">Read more</a>
                </div> -->
            </section>
            <section id="instagram">
                <h2>Instagram</h2>
                <ul class="photos"></ul>
            </section>
        </main>
        <?php get_footer(); ?>
