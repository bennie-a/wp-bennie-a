<?php get_header(); ?>
    <div class="contents">
        <main role="main">
           <div class="hero">
              <?php if(has_post_thumbnail()): ?>
                <div class="hero-img" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id()); ?>);"></div>
              <?php endif; ?>
                <div class="hero-img-mask">
                  <div class="hero-text">
                      <?php the_tags('<ul class="tag"><li>', '</li><li>', '</li></ul>'); ?>
                      <?php the_title('<h1>', '</h1>'); ?>
                  </div>
                </div>
           </div>
          <?php if(have_posts()): while(have_posts()): the_post(); ?>
              <?php the_content(); ?>
          <?php endwhile; endif; ?>
          <nav id="footer-nav">
            <section>
              <h2>Other Project</h2>
              <ul class="prev-next">
                <?php
                  $prevpost = get_adjacent_post(false, '', true);
                  $previd;
                  if ($prevpost) {
                    $previd = $prevpost->ID;
                  } else {
                    $args = array('posts_per_page' => 1, 'orderby' => 'date', 'order' => 'DESC');
                    $latestpost = get_posts( $args )[0];
                    $previd = $latestpost->ID;
                    wp_reset_postdata();
                  }
               ?>
                <li>
                  <?php echo get_post_link($previd); ?>
                </li>
                <?php
                  $nextpost = get_adjacent_post(false, '', false);
                  $nextid;
                  if ($nextpost) {
                    $nextid = $nextpost->ID;
                  } else {
                    $args = array('posts_per_page' => 1, 'orderby' => 'date', 'order' => 'ASC');
                    $firstpost = get_posts( $args )[0];
                    $nextid = $firstpost->ID;
                  }
                ?>
                <li>
                  <?php echo get_post_link($nextid); ?>
                </li>
              </ul>
            </section>
          </nav>
        </main>
        <?php get_footer(); ?>
