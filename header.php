<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://bennie-a.com">
    <meta charset="UTF-8">
    <meta name="viewport"
     content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/font-awesome/css/font-awesome.min.css">
     <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/reset.min.css">
     <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
    <?php if(is_single()): ?>
      <meta property="og:type" content="article">
      <meta property="og:title" content="<?php the_title(); ?>">
      <meta property="og:url" content="<?php the_permalink(); ?>">
      <meta property="og:description" content="<?php echo wp_trim_words($post -> post_content, 100, '...'); ?>">
      <?php if(has_post_thumbnail()): ?>
        <?php $postthumb = wp_get_attachment_image_src(get_post_thumbnail_id); ?>
        <meta name="og:image" content="<?php echo $postthumb; ?>">
      <?php endif; ?>
    <?php endif; ?>
    <?php
      wp_deregister_script('jquery');
      wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', array(), '1.9.1');

     ?>
    <?php wp_head(); ?>
</head>
<body>
<header id="top-head">
   <div class="inner">
       <div id="mobile-head">
            <a href="<?php echo home_url(); ?>"><img src="<?php echo common_img_path(); ?>/logo.svg" alt="Bennie-Aトップページへ"></a>
            <div id="nav-toggle">
              <div>
                <label for="navi" class="menu-icon">MENU</label>
                <span></span>
                <span></span>
                <span></span>
              </div>
          </div>
       </div>
       <nav role="navigation" id="global-nav">
        <?php wp_nav_menu(array(
            'theme_location' => 'sitenav'
        )); ?>
        </nav>
   </div>
</header>
